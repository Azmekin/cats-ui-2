import { test, expect } from '@playwright/test';
import { unsuccessResponse } from '../__mocks__/unsuccess-response';

test('При неуспешном ответе API отображается Ошибка загрузки рейтинга', async ({
  page,
}) => {
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        json: unsuccessResponse,
      });
    }
  );

  await page.goto(
    '/rating'
  );
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});
