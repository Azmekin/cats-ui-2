import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики с рейтинговой страницей приложения котиков.
 *
 */
export class MainPage {
  private page: Page;
  public buttonSelector: string
  public blockurl: string

  constructor({
    page,
   }: {
    page: Page;
  }) {
    this.page = page;
    this.blockurl = '/api/likes/cats/rating';
  }

  async blockurl_f(){
    
    return await test.step('block url', async () => {
      await this.page.route(
        request => request.href.includes('/api/likes/cats/rating'),
        async route => {
          route.abort("failed")
        })
    })
  }

  async openMainPage() {
    return await test.step('Открываю рейтингову страницу приложения', async () => {
      await this.page.goto('/rating')
    })
  }

  async getElementsChech(){
    return await test.step('Сравнение значений', async () => {
      await this.page.locator
    })
  }


}

export type MainPageFixture = TestFixture<
  MainPage,
  {
    page: Page;
  }
  >;

export const mainPageFixture: MainPageFixture = async (
  { page },
  use
) => {
  const mainPage = new MainPage({ page });

  await use(mainPage);
};

