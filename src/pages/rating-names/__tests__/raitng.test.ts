import { test, expect } from '@playwright/test';
import { unsuccessResponse } from '../__mocks__/unsuccess-response';

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page   }) => {
  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      route.abort("failed")
    }
  );

  await page.goto(
    '/rating'    
  );
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test.skip('Рейтинг котиков отображается', async ({ page   }) => {
  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */
});