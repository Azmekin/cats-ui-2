import { test as base, expect,request } from '@playwright/test';
import { MainPage, mainPageFixture} from '../__page-rating-object__'
import { string } from 'prop-types';


const test = base.extend<{ mainPage: MainPage }>({
  mainPage: mainPageFixture,
});

test('Появляется текст ошибки подключения на странице рейтинга', async ({ page, mainPage }) => {
  await mainPage.blockurl_f()
  await mainPage.openMainPage()
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});


test('Проверка сортировки', async ({ page, mainPage }) => {
  const context = await request.newContext({
    baseURL: 'https://meowle.fintech-qa.ru/',})

  const issues = await context.get(`/api/likes/cats/rating`);
  expect(issues.ok()).toBeTruthy();
  let like,like1,like2;
  like=await issues.json()
  like1=like.likes[1].likes
  like2=like.likes[0].likes
  expect(like1).toBeLessThanOrEqual(like2)

});
